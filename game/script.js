// fonction qui initialise le nombre de cartes par rapport à leurs valeurs
function card(count, values = []) {
    let cards = [];
    values.forEach(function(value) {
        for (let i = 0; i < count; i++) {
            cards.push(value);
        }
    });
    return cards;
}
// fonction qui initialise le tas de cartes numérotées
function getNumberCards() {
    return card(3, [1, 2, 14, 15])
        .concat(card(4, [3, 13]))
        .concat(card(5, [4, 12]))
        .concat(card(6, [5, 11]))
        .concat(card(7, [6, 10]))
        .concat(card(8, [7, 9]))
        .concat(card(9, [8]));
}
// fonction qui initialise la tas de cartes illustrées
function getDrawingCards() {
    return card(6, ["agence_interim.png"])
        .concat(card(9, ["agent_immobilier.png"]))
        .concat(card(12, ["geometre.png"]))
        .concat(card(15, ["numero_bis.png"]))
        .concat(card(18, ["paysagiste.png"]))
        .concat(card(21, ["piscine.png"]));
}
// fonction qui tri les cartes en piles
function makePile(nb, cards) {
    let piles = [];
    for (let i = 0; i < nb; i++) {
        piles[i] = [];
    }

    while (cards.length > 0) {
        for (let i = 0; i < nb; i++) {
            const card = cards.splice(Math.random() * (cards.length - 1), 1);
            piles[i].push(card[0]);
        }
    }
    return piles;
}

// fonction qui permet de changer de cartes
function click() {
    let trash = [];
    let categories = [numberCardsPile, drawingCardsPile];
    categories.map(function(category) {
        category = category.map(pile => {
            let trashCard = pile.pop();
            trash.push(trashCard);
            return pile;
        });
    });

    let cardsNumber = document.querySelectorAll(".cardNumber>.card");

    for (let index = 0; index < cardsNumber.length; index++) {
        let cards = numberCardsPile[index];
        let lastCard = cards[cards.length - 1];
        console.log(lastCard);
        if (lastCard === undefined) {
            cardsNumber[index].style.display = "none";
        } else {
            cardsNumber[index].querySelector("span").innerHTML = lastCard;
        }
    }

    let cardsImage = document.querySelectorAll(".cardImage>.card");

    for (let index = 0; index < cardsImage.length; index++) {
        let cards = drawingCardsPile[index];
        let lastCard = cards[cards.length - 1];
        console.log(lastCard);
        if (lastCard === undefined) {
            cardsImage[index].style.display = "none";
        } else {
            cardsImage[index].querySelector("img").setAttribute("src", "../img/" + lastCard);
        }
    }
}

let btn = document.getElementById("btn");
let btn2b= document.getElementById("btn2");
btn.addEventListener("click", click);
btn2.addEventListener("click", click);
let numberCardsPile = makePile(3, getNumberCards());
let drawingCardsPile = makePile(3, getDrawingCards());